package org.okrek.dummyapp.services

import org.okrek.dummyapp.modules.main.MainScreenScope
import javax.inject.Inject

/**
 * Created by hiiamfrankie on 24.07.17.
 */

@MainScreenScope
class SharedCounter @Inject constructor() {
    var value = 0
}