package org.okrek.dummyapp.services

import org.okrek.dummyapp.di.qualifiers.DefaultErrorConsumer
import org.okrek.dummyapp.di.qualifiers.LocalConsumers
import javax.inject.Inject

/*
 * Created by Maxim Pisarenko on 17.08.2017.
 */

class ErrorResolver @Inject constructor(

    private val globalConsumers: Set<@JvmSuppressWildcards IErrorConsumer>,

    @LocalConsumers
    private val localConsumers: Set<@JvmSuppressWildcards IErrorConsumer>,

    @DefaultErrorConsumer
    private val defaultConsumer: IErrorConsumer) {

    fun resolve(throwable: Throwable) {
        if (localConsumers.any { it.consume(throwable) }) return
        if (globalConsumers.any { it.consume(throwable) }) return

        defaultConsumer.consume(throwable)
    }
}