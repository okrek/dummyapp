package org.okrek.dummyapp.services

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by hiiamfrankie on 23.07.17.
 */

@Singleton
class ResourcesCounterService @Inject constructor(

    private val context: Context) {

    fun getInt(stringResId: Int): Int {
        return context.getString(stringResId).toInt()
    }
}