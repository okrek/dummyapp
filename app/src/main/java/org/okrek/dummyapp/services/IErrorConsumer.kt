package org.okrek.dummyapp.services

import android.app.Activity
import android.content.Context
import dagger.Lazy
import org.okrek.dummyapp.modules.main.firstscreen.ISecondScreenView
import java.io.IOException
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by Maxim Pisarenko on 16.08.2017.
 */

class GlobalErrorConsumer @Inject constructor(activity: Activity,
                                              val weakActivity: WeakReference<Activity> = WeakReference(activity)) : IErrorConsumer {

    override fun consume(throwable: Throwable): Boolean {
        if (throwable is IOException) {
            val activity = weakActivity.get()
            activity?.let {

            }

            return true
        }

        return false
    }
}

class DefaultConsumer @Inject constructor(private val context: Context) : IErrorConsumer {
    override fun consume(throwable: Throwable): Boolean {
        return true
    }
}

class LocalErrorConsumer @Inject constructor() : IErrorConsumer {
    override fun consume(throwable: Throwable): Boolean {
        return false
    }
}

interface IErrorConsumer {
    fun consume(throwable: Throwable): Boolean
}

class SecondLocalErrorConsumer @Inject constructor() : IErrorConsumer {

    @Inject lateinit var viewState: Lazy<ISecondScreenView>

    override fun consume(throwable: Throwable): Boolean {
        val viewState = viewState.get() ?: return false
        viewState.showCounterValue(666)

        return true
    }
}