package org.okrek.dummyapp.modules.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by hiiamfrankie on 19.07.17.
 */

interface IMainView : MvpView {

    fun showGreeting(greeting: String)

    @StateStrategyType(SkipStrategy::class)
    fun navigateToFirstScreen()

    @StateStrategyType(SkipStrategy::class)
    fun navigateToSecondScreen()
}