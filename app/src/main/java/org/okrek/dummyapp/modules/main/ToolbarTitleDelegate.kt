package org.okrek.dummyapp.modules.main

/**
 * Created by hiiamfrankie on 20.07.17.
 */

interface ToolbarTitleDelegate {

    fun setTitle(title: String)

    fun restoreTitle()
}