package org.okrek.dummyapp.modules.main

import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import org.okrek.dummyapp.R
import org.okrek.dummyapp.common.BaseMvpActivity
import org.okrek.dummyapp.modules.main.firstscreen.FirstScreenFragment
import org.okrek.dummyapp.modules.main.secondscreen.SecondScreenFragment

class MainActivity : BaseMvpActivity<MainPresenter>(), ToolbarTitleDelegate, IMainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    override fun providePresenter(): MainPresenter {
        return presenterProvider.get()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun restoreTitle() {
        supportActionBar?.title = "Main Activity"
    }

    override fun showGreeting(greeting: String) {
        messageTextView.text = greeting
    }

    override fun navigateToFirstScreen() {
        showFragment(FirstScreenFragment())
    }

    override fun navigateToSecondScreen() {
        showFragment(SecondScreenFragment())
    }

    private fun initViews() {

        greetButton.setOnClickListener {
            presenter.onGreetButtonClick()
        }

        showFirstScreenButton.setOnClickListener {
            presenter.onShowFirstScreenClick()
        }

        showSecondScreenButton.setOnClickListener {
            presenter.onShowSecondScreenClick()
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .addToBackStack(null)
            .commit()
    }
}
