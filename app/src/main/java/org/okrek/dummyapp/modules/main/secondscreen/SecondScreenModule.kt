package org.okrek.dummyapp.modules.main.secondscreen

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import org.okrek.dummyapp.di.qualifiers.LocalConsumers
import org.okrek.dummyapp.modules.main.firstscreen.ISecondScreenView
import org.okrek.dummyapp.services.IErrorConsumer
import org.okrek.dummyapp.services.SecondLocalErrorConsumer

/**
 * Created by Maxim Pisarenko on 09.08.2017.
 */

@Module
class SecondScreenModule {

    @Provides
    fun provideInteractor(): Interactor {
        return Interactor()
    }

    @Provides
    fun provideSecondScreenView(presenter: SecondScreenPresenter): ISecondScreenView {
        return presenter.viewState
    }

    @Provides
    @IntoSet
    @LocalConsumers
    fun provideSecondLocalErrorConsumer(secondLocalErrorConsumer: SecondLocalErrorConsumer): IErrorConsumer {
        return secondLocalErrorConsumer
    }
}