package org.okrek.dummyapp.modules.main.firstscreen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import org.okrek.dummyapp.R
import org.okrek.dummyapp.services.ResourcesCounterService
import org.okrek.dummyapp.services.SharedCounter
import javax.inject.Inject

/**
 * Created by hiiamfrankie on 24.07.17.
 */

@InjectViewState
class FirstScreenPresenter @Inject constructor(

    private val resourcesCounterService: ResourcesCounterService,
    private val sharedCounter: SharedCounter) : MvpPresenter<IFirstScreenView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showCounterValue(sharedCounter.value)
    }

    fun onSetValueFromResourcesClick() {
        sharedCounter.value = resourcesCounterService.getInt(R.string.first_screen_counter_value)
        viewState.showCounterValue(sharedCounter.value)
    }

    fun onIncrementCounterClick() {
        sharedCounter.value++
        viewState.showCounterValue(sharedCounter.value)
    }

    fun onDecrementCounterClick() {
        sharedCounter.value--
        viewState.showCounterValue(sharedCounter.value)
    }
}