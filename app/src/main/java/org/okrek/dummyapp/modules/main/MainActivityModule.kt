package org.okrek.dummyapp.modules.main

import android.app.Activity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoSet
import org.okrek.dummyapp.di.ScreenScope
import org.okrek.dummyapp.di.qualifiers.LocalConsumers
import org.okrek.dummyapp.modules.main.firstscreen.FirstScreenFragment
import org.okrek.dummyapp.modules.main.secondscreen.SecondScreenFragment
import org.okrek.dummyapp.modules.main.secondscreen.SecondScreenModule
import org.okrek.dummyapp.services.IErrorConsumer
import org.okrek.dummyapp.services.LocalErrorConsumer

/**
 * Created by hiiamfrankie on 19.07.17.
 */

@Module
interface MainActivityModule {

    @Binds
    fun bindToolbarTitleDelegate(mainActivity: MainActivity): ToolbarTitleDelegate

    @Binds
    fun bindActivity(mainActivity: MainActivity): Activity

    @Binds
    @IntoSet
    @LocalConsumers
    fun bindLocalErrorConsumer(localErrorConsumer: LocalErrorConsumer): IErrorConsumer

    @ContributesAndroidInjector
    fun bindFirstScreenFragment(): FirstScreenFragment

    @ScreenScope
    @ContributesAndroidInjector(modules = arrayOf(SecondScreenModule::class))
    fun bindSecondScreenFragment(): SecondScreenFragment
}