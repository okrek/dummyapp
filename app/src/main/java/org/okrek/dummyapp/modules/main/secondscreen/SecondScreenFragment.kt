package org.okrek.dummyapp.modules.main.secondscreen

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_counter.*
import org.okrek.dummyapp.R
import org.okrek.dummyapp.common.BaseMvpFragment
import org.okrek.dummyapp.modules.main.ToolbarTitleDelegate
import org.okrek.dummyapp.modules.main.firstscreen.ISecondScreenView
import javax.inject.Inject

/**
 * Created by hiiamfrankie on 20.07.17.
 */

class SecondScreenFragment : BaseMvpFragment<SecondScreenPresenter>(), ISecondScreenView {

    @Inject
    lateinit var toolbarTitleDelegate: ToolbarTitleDelegate

    @InjectPresenter
    lateinit var presenter: SecondScreenPresenter

    @ProvidePresenter
    override fun providePresenter(): SecondScreenPresenter {
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_counter, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDetach() {
        super.onDetach()
        toolbarTitleDelegate.restoreTitle()
    }

    override fun showCounterValue(value: Int) {
        counterTextView.text = "Count: $value"
        AlertDialog.Builder(activity).setTitle("Yo!").setMessage(value.toString()).show()
    }

    private fun initViews() {
        toolbarTitleDelegate.setTitle(getString(R.string.second_screen_title))

        decrementButton.setOnClickListener {

        }

        incrementButton.setOnClickListener {
            presenter.onPlusClick()
        }

        setFromResourcesButton.setOnClickListener {

        }
    }
}