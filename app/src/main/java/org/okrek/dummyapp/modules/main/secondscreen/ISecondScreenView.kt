package org.okrek.dummyapp.modules.main.firstscreen

import com.arellomobile.mvp.MvpView

/**
 * Created by hiiamfrankie on 24.07.17.
 */

interface ISecondScreenView : MvpView {

    fun showCounterValue(value: Int)
}