package org.okrek.dummyapp.modules.main.secondscreen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import org.okrek.dummyapp.di.ScreenScope
import org.okrek.dummyapp.modules.main.firstscreen.ISecondScreenView
import org.okrek.dummyapp.services.ErrorResolver
import javax.inject.Inject

/**
 * Created by Maxim Pisarenko on 09.08.2017.
 */
@InjectViewState
@ScreenScope
class SecondScreenPresenter @Inject constructor() : MvpPresenter<ISecondScreenView>() {

    @Inject lateinit var interactor: Interactor
    @Inject lateinit var errorResolver: ErrorResolver

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

     fun onPlusClick() {
        errorResolver.resolve(Exception())
//        viewState.showCounterValue(999)
    }
}

class Interactor
