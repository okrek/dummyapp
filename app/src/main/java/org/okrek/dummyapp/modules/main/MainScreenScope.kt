package org.okrek.dummyapp.modules.main

import javax.inject.Scope

/**
 * Created by hiiamfrankie on 19.07.17.
 */
@Scope
annotation class MainScreenScope
