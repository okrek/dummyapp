package org.okrek.dummyapp.modules.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import org.okrek.dummyapp.services.ErrorResolver
import javax.inject.Inject

/**
 * Created by hiiamfrankie on 19.07.17.
 */


@InjectViewState
class MainPresenter @Inject constructor() : MvpPresenter<IMainView>() {

    @Inject
    lateinit var errorResolver: ErrorResolver

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    fun onGreetButtonClick() {
        viewState.showGreeting("Hello!")
    }

    fun onShowFirstScreenClick() {
        viewState.navigateToFirstScreen()
    }

    fun onShowSecondScreenClick() {
        viewState.navigateToSecondScreen()
    }
}