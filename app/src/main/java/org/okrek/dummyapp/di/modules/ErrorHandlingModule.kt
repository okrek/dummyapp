package org.okrek.dummyapp.di.modules

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import dagger.multibindings.Multibinds
import org.okrek.dummyapp.di.qualifiers.DefaultErrorConsumer
import org.okrek.dummyapp.di.qualifiers.LocalConsumers
import org.okrek.dummyapp.services.DefaultConsumer
import org.okrek.dummyapp.services.GlobalErrorConsumer
import org.okrek.dummyapp.services.IErrorConsumer

/**
 * Created by Maxim Pisarenko on 17.08.2017.
 */

@Module(includes = arrayOf(ErrorHandlingModule.Declarations::class))
class ErrorHandlingModule {

    @Provides
    @IntoSet
    fun provideGlobalExceptionConsumer(activity: Activity): IErrorConsumer {
        return GlobalErrorConsumer(activity)
    }


    @Provides
    @DefaultErrorConsumer
    fun provideDefaultExceptionConsumer(context: Context): IErrorConsumer {
        return DefaultConsumer(context)
    }

    @Module
    interface Declarations {

        @Multibinds
        @LocalConsumers
        fun defaultLocalConsumersSet(): Set<IErrorConsumer>
    }
}