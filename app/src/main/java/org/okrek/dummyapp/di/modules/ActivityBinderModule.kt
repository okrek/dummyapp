package org.okrek.dummyapp.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.okrek.dummyapp.modules.main.MainActivity
import org.okrek.dummyapp.modules.main.MainActivityModule
import org.okrek.dummyapp.modules.main.MainScreenScope


/**
 * Created by hiiamfrankie on 19.07.17.
 */

@Module
interface ActivityBinderModule {

    @MainScreenScope
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class, ErrorHandlingModule::class))
    fun bindMainActivity(): MainActivity

}