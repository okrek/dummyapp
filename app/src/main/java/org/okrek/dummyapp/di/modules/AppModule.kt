package org.okrek.dummyapp.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides


/**
 * Created by hiiamfrankie on 23.07.17.
 */

@Module
class AppModule {

    @Provides
    fun provideContext(application: Application): Context {
        return application
    }
}