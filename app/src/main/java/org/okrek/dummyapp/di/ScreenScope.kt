package org.okrek.dummyapp.di

import javax.inject.Scope

/**
 * Created by Maxim Pisarenko on 17.08.2017.
 */

@Scope
annotation class ScreenScope
