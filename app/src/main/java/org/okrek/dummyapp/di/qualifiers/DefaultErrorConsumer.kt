package org.okrek.dummyapp.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by Maxim Pisarenko on 17.08.2017.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DefaultErrorConsumer