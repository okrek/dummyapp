package org.okrek.dummyapp.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import org.okrek.dummyapp.DummyApp
import org.okrek.dummyapp.di.modules.ActivityBinderModule
import org.okrek.dummyapp.di.modules.AppModule
import javax.inject.Singleton

/**
 * Created by hiiamfrankie on 19.07.17.
 */

@Singleton
@Component(modules = arrayOf(
    AndroidInjectionModule::class,
    ActivityBinderModule::class,
    AppModule::class))
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: DummyApp)
}

