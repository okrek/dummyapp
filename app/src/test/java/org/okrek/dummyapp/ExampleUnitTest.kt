package org.okrek.dummyapp

import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mockito.`when` as _when

/**
 * Example local unit test, which will execute on the development machine (host).

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ExampleUnitTest {

    @Test
    fun rxtest() {

        val test = NullableHolder(FirstLevelNullable(SecondLevelNullable(null)))
//        val result = test.firstLevelNullable?.secondLevelNullable?.value

        val observer = Observable.just(NullableHolder(FirstLevelNullable(null)))
            .filter { it.firstLevelNullable?.secondLevelNullable?.value != null }
            .test()

//        print(observer.values())
        filter(test) {
            it.firstLevelNullable?.secondLevelNullable?.value != null
        }

    }

    inline fun filter(holder: NullableHolder, block: (NullableHolder) -> Boolean) {
        block.invoke(holder)
    }

    inline fun map(holder: NullableHolder, block: (NullableHolder) -> String) {
        block.invoke(holder)
    }
}

class FirstLevelNullable(val secondLevelNullable: SecondLevelNullable?)

class SecondLevelNullable(val value: String?)

class NullableHolder(val firstLevelNullable: FirstLevelNullable?)